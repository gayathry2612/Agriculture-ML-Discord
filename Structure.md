# Structure of the program 

An overview. We adapt and modify as we continue and based on how earnest & proactive we are as a collective. 

Phase 1 : Research the problems that are persisting in agriculture. Go to the fundamental issues, speak to the humans, build your points , conduct debate , do your own ground research. Analyse why it should be solved. At this point we are not worried about "How to use ML , IOT, APPS / Any technology". Convince others to join your cause - that it is worthy to solve. We identify issues by the end of 4 weeks.  Based on what problem you feel strongly and identify with, you will be joining that group. 

Phase 2 : Prototyping mode. The group you join , we will try to make it self-sufficient, based on the resources. You create a plan, discuss the nitti-gritties on how the prototype of the solution will look like. For code and concept sharing you will be using Github. Wherever needed we will support you
