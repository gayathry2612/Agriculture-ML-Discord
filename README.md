# Agriculture &amp; AI Wiki page
Thank you for your interest in contributing to open source. 

1. If you are new to github, please learn it. Create a repo and experiment with it. Watch tutorials on atlassian, youtube. Understand all the basic terminologies.

2. Introduce yourselves on this channel #introduce-yourself . Your name, where you are from, tech stack (Python, Java, etc.). Share your thoughts on agriculture sector, machine learning. What do you want to build.

3. Find a channel #front-end-dev , #backend-dev etc. and join it. Create a channel if needed. Later it will help us in understanding your interests, who will work mainly in that area.

4. Everyone here is from different backgrounds. It's a heterogenous mix, and that is good for a bigger project. Focus on finding the right problems to solve - that's where diverse backgrounds help. We will somehow engage you in the prototype.

5. Be proactive in discussions. Your time and our time is precious. This project aims to discover problems, build use cases, and launch proof of concepts.

6. Contribution is key. You will own a part of this project, once we are live. We keep things transparent and open for that reason.

7. Groups : The finalised members will be sorted in groups. Each group will be self-sufficient to launch a prototype. We attempt to create a codebase on github, documentation, supporting video to explain the use case at the end of 8 weeks.

8. Last : We need atleast 10 hrs of contribution everyweek. The more the merrier. The contributions hours are based on honor-code. We trust you to be the change in the world. You decide how you want to do it.
